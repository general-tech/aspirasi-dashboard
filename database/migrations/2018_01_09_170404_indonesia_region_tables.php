<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class IndonesiaRegionTables extends Migration
{
    public function up()
    {
        Schema::create('provinces', function (Blueprint $table) {
            $table->char('id', 2);
            $table->string('name', 255);

            $table->primary('id');
        });

        Schema::create('regencies', function (Blueprint $table) {
            $table->char('id', 4);
            $table->char('province_id', 2);
            $table->string('name', 255);

            $table->primary('id');
            $table->foreign('province_id')->references('id')->on('provinces');
        });

        Schema::create('districts', function (Blueprint $table) {
            $table->char('id', 7);
            $table->char('regency_id', 4);
            $table->string('name', 255);

            $table->primary('id');
            $table->foreign('regency_id')->references('id')->on('regencies');
        });

        Schema::create('villages', function (Blueprint $table) {
            $table->char('id', 10);
            $table->char('district_id', 4);
            $table->string('name', 255);

            $table->primary('id');
            $table->foreign('district_id')->references('id')->on('districts');
        });

        Schema::table('users', function (Blueprint $table) {
            $table->foreign('regency_id')->references('id')->on('regencies');
        });
    }

    public function down()
    {
        // Schema::dropIfExists('villages');
        // Schema::dropIfExists('districts');
        // Schema::dropIfExists('regencies');
        // Schema::dropIfExists('provinces');
    }
}
