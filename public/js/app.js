$(document).ready(function() {
    $('.date-picker').datepicker({
        rtl: App.isRTL(),
        orientation: 'left',
        autoclose: true,
        format: 'd M yyyy'
    });
});

function trash(data) {
    uuid = $(data).attr('data-id');
    module = $(data).attr('data-module');

    if (confirm('Apakah Anda yakin menghapus ' + module + ' ini?')) { // Apakah Anda yakin menghapus Kebijakan ini?
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            url: window.location.pathname + '/destroy',
            type: 'PATCH',
            data: {
                'uuid': uuid
            },
            success: function() {
                window.location.replace(window.location.pathname);
            }
        });
    }
}

function disabledInput() {
    $('.form-control').attr('disabled', true);
}
