<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\News;

use Auth;
use Cloudder;
use Entrust;
use File;

class NewsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');

        $this->data['module'] = 'Berita';
        $this->data['breadcrumbs'] = getBreadcrumb();
    }

    public function index()
    {
        $this->data['title'] = 'Daftar ' . $this->data['module'];
        $this->data['news'] = (
            News::where('status', '&', 3)
                ->orderBy('created_at', 'DESC')
                ->paginate(10)
        );

        return view('pages.news.news-index', $this->data);
    }

    public function create()
    {
        $this->data['title'] = 'Tambah ' . $this->data['module'];
        $this->data['method'] = 'POST';
        $this->data['theNews'] = new News;

        return view('pages.news.news-form', $this->data);
    }

    public function store(Request $request)
    {
        $messages = [
            'title.required'       => 'Judul dibutuhkan',
            'description.required' => 'Deskripsi dibutuhkan',
            'thumbnail.required'   => 'Gambar dibutuhkan'
        ];

        $this->validate($request, [
            'title'       => 'required',
            'description' => 'required',
            'thumbnail'   => 'required|image|mimes:jpeg,png|file|max:1024'
        ], $messages);

        $request['uuid'] = getUUID();

        if ($request->file('thumbnail')) {
            Cloudder::upload(
                $request->file('thumbnail'),
                $publicId = 'berita/' . $request->uuid
            );

            $request['thumbnail_id'] = Cloudder::getResult()['public_id'];
            $request['thumbnail_url'] = Cloudder::getResult()['url'];
        }

        if ($request->description) {
            $dom = new \DomDocument();
            libxml_use_internal_errors(true);
            $dom->loadHTML($request->description, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
            libxml_clear_errors();
            $images = $dom->getElementsByTagName('img');

            foreach($images as $k => $img) {
                $data = $img->getAttribute('src');

                if (count(explode(';', $data)) > 1) {
                    list($type, $data) = explode(';', $data);
                    list(, $data) = explode(',', $data);

                    $data = base64_decode($data);
                    $tmp_name = '/tmp/' . time() . $k . '.jpg';
                    $path = public_path('/img') . $tmp_name;

                    file_put_contents($path, $data);

                    Cloudder::upload(
                        $path,
                        $publicId = ''
                    );

                    $img_id = Cloudder::getResult()['public_id'];

                    $img->removeAttribute('data-id');
                    $img->setAttribute('data-id', $img_id);
                    $img_url = Cloudder::show($img_id);

                    File::delete($path);
                } else {
                    $img_url = $data;
                }

                $img->removeAttribute('data-filename');
                $img->removeAttribute('style');
                $img->removeAttribute('src');
                $img->setAttribute('src', $img_url);
            }

            $description = $dom->saveHTML();

            $request['description'] = $description;
        }

        $request['created_by'] = Auth::id();
        $request['updated_by'] = Auth::id();

        News::create($request->all());

        return redirect()
            ->route('berita')
            ->with('status', 'success')
            ->with('text', 'Berhasil menambah ' . $this->data['module'] . ' baru');
    }

    public function show(Request $request)
    {
        $this->data['title'] = 'Lihat ' . $this->data['module'];
        $this->data['method'] = 'PATCH';
        $this->data['theNews'] = (
            News::where('uuid', $request->uuid)
                ->where('status', '&', 3)
                ->firstOrFail()
        );

        return view('pages.news.news-form', $this->data);
    }

    public function edit(Request $request)
    {
        $this->data['title'] = 'Ubah ' . $this->data['module'];
        $this->data['method'] = 'PATCH';
        $this->data['theNews'] = (
            News::where('uuid', $request->uuid)
                ->where('status', '&', 3)
                ->firstOrFail()
        );

        return view('pages.news.news-form', $this->data);
    }

    public function update(Request $request)
    {
        $messages = [
            'title.required'       => 'Judul dibutuhkan',
            'description.required' => 'Deskripsi dibutuhkan',
            'thumbnail.required'   => 'Gambar dibutuhkan'
        ];

        $this->validate($request, [
            'title'       => 'required',
            'description' => 'required',
            'thumbnail'   => 'sometimes|nullable|image|mimes:jpeg,png|file|max:1024'
        ]);

        if ($request->file('thumbnail')) {
            Cloudder::delete('berita/' . $request->uuid);
            Cloudder::upload(
                $request->file('thumbnail'),
                $publicId = 'berita/' . $request->uuid
            );

            $request['thumbnail_id'] = Cloudder::getResult()['public_id'];
            $request['thumbnail_url'] = Cloudder::getResult()['url'];
        }

        if ($request->description) {
            $dom = new \DomDocument();
            libxml_use_internal_errors(true);
            $dom->loadHTML($request->description, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
            libxml_clear_errors();
            $images = $dom->getElementsByTagName('img');

            foreach($images as $k => $img) {
                $data = $img->getAttribute('src');

                if (count(explode(';', $data)) > 1) {
                    list($type, $data) = explode(';', $data);
                    list(, $data) = explode(',', $data);

                    $data = base64_decode($data);
                    $tmp_name = '/tmp/' . time() . $k . '.jpg';
                    $path = public_path('/img') . $tmp_name;

                    file_put_contents($path, $data);

                    Cloudder::upload(
                        $path,
                        $publicId = ''
                    );

                    $img_id = Cloudder::getResult()['public_id'];

                    $img->removeAttribute('data-id');
                    $img->setAttribute('data-id', $img_id);
                    $img_url = Cloudder::show($img_id);

                    File::delete($path);
                } else {
                    $img_url = $data;
                }

                $img->removeAttribute('data-filename');
                $img->removeAttribute('style');
                $img->removeAttribute('src');
                $img->setAttribute('src', $img_url);
            }

            $description = $dom->saveHTML();

            $request['description'] = $description;
        }

        $request['updated_by'] = Auth::id();

        News::where('uuid', $request->uuid)
            ->firstOrFail()
            ->update($request->all());

        return redirect()
            ->route('berita')
            ->with('status', 'success')
            ->with('text', 'Berhasil mengubah ' . $this->data['module']);
    }

    public function destroy(Request $request)
    {
        $theNews = News::where('uuid', $request->uuid)->first();

        if ($theNews === null) {
            $request->session()->flash('status', 'danger');
            $request->session()->flash('text', 'Gagal untuk menghapus ' . $this->data['module']);
        } else {
            $theNews->update([
                'status'     => 4,
                'updated_by' => Auth::id()
            ]);

            $request->session()->flash('status', 'success');
            $request->session()->flash('text', 'Berhasil menghapus ' . $this->data['module']);
        }
    }
}
