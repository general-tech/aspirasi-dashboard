<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFigureUserTable extends Migration
{
    public function up()
    {
        Schema::create('figure_user', function (Blueprint $table) {
            $table->integer('figure_id')->unsigned();
            $table->integer('user_id')->unsigned();

            $table->foreign('figure_id')->references('id')->on('figures')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->primary(['figure_id', 'user_id']);
        });
    }

    public function down()
    {
        Schema::dropIfExists('figure_user');
    }
}
