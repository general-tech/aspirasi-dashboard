@extends('layouts.app')

@section('content')
    @if (session('status'))
        <div class="note note-{{ session('status') }}">
            <p>{{ session('text') }}</p>
        </div>
        <script type="text/javascript">
            setTimeout(function() {
                $('div.note').slideUp();
            }, 3000);
        </script>
    @endif

    <div class="portlet box grey-gallery">
        <div class="portlet-title">
            <div class="caption">{{ $title }}</div>
            <div class="actions">
                <a href="{{ route('berita.create') }}" class="btn btn-lg green-meadow">
                    Tambah Baru <i class="fa fa-plus"></i>
                </a>
            </div>
        </div>
        <div class="portlet-body">
            <div class="table-responsive">
                <table class="table table-bordered table-striped table-hover">
                    <thead>
                        <tr>
                            <th width="50">#</th>
                            <th>Judul</th>
                            <th width="80" class="text-center">Status</th>
                            @if (Auth::user()->hasRole(['admin', 'editor']))
                                <th width="50"></th>
                            @endif
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($news as $key => $theNews)
                            <tr>
                                <td>{{ ++$key + (($news->currentPage() - 1) * $news->perPage()) }}</td>
                                <td><a href="{{ route('berita.show', ['uuid' => $theNews->uuid]) }}">{{ $theNews->title }}</a></td>
                                <td align="middle">
                                    @if ($theNews->status == 1)
                                        <span class="label label bg-blue">Terbit</span>
                                    @elseif ($theNews->status == 2)
                                        <span class="label label bg-yellow-casablanca">Draf</span>
                                    @endif
                                </td>
                                @if (Auth::user()->hasRole(['admin', 'editor']))
                                    <td align="middle">
                                        <span class="font-lg font-red" role="button" data-id="{{ $theNews->uuid }}" data-module="{{ $module }}"
                                            onclick="event.preventDefault()
                                                trash(this)">
                                            <i class="fa fa-trash"></i>
                                        </span>
                                    </td>
                                @endif
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            {{ $news->links() }}
        </div>
    </div>
@endsection
