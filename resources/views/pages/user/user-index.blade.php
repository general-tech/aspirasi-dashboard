@extends('layouts.app')

@section('content')
    @if (session('status'))
        <div class="note note-{{ session('status') }}">
            <p>{{ session('text') }}</p>
        </div>
        <script type="text/javascript">
            setTimeout(function() {
                $('div.note').slideUp();
            }, 3000);
        </script>
    @endif

    <div class="portlet box grey-gallery">
        <div class="portlet-title">
            <div class="caption">{{ $title }}</div>
        </div>
        <div class="portlet-body">
            <div class="table-responsive">
                <table class="table table-bordered table-striped table-hover">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th width="110" class="text-center">Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($users as $key => $user)
                            <tr>
                                <td>{{ ++$key + (($users->currentPage() - 1) * $users->perPage()) }}</td>
                                <td><a href="{{ route('user.show', ['uuid' => $user->uuid]) }}">{{ $user->name }}</a></td>
                                <td>{{ $user->email }}</td>
                                <td class="status" align="middle">
                                    @if ($user->status == 1)
                                        <span class="label label-success font-default bold">Approve</span>
                                    @elseif ($user->status == 2)
                                        <span class="label label-danger font-default bold" onclick="approve(this);" role="button" data-id="{{ $user->uuid }}">Unapprove</span>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            {{ $users->links() }}
        </div>
    </div>
@endsection

@section('script')
<script type="text/javascript">
    function approve(data) {
        if (confirm('Setujui Akun ini?')) {
            uuid = $(data).data('id');

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                url: '{{ route('user.update') }}',
                type: 'PATCH',
                data: {
                    'uuid': uuid
                },
                success: function(result) {
                    $(data).remove();
                    $('.status').append("<span class='label label-success font-default bold'>Approve</span>");
                }
            });
        }
    }
</script>
@endsection
