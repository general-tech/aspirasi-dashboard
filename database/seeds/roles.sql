INSERT INTO `roles` (`uuid`, `name`, `display_name`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(UUID(), 'admin', 'Admin', 1, 1, 1, NOW(), NOW()),
(UUID(), 'contributor', 'Contributor', 1, 1, 1, NOW(), NOW()),
(UUID(), 'editor', 'Editor', 1, 1, 1, NOW(), NOW());
