<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    public function run()
    {
        App\User::truncate();

        App\User::insert([
            [
                'id'          => 1,
                'uuid'        => getUUID(),
                'avatar'      => NULL,
                'name'        => 'Triadi Arifin',
                'username'    => 'triadi',
                'email'       => 'triadi@sti-car.com',
                'password'    => bcrypt('secret'),
                'birth_place' => NULL,
                'birth_date'  => NULL,
                'gender'      => 'male',
                'phone'       => NULL,
                'regency_id'  => NULL,
                'isAdmin'     => 1,
                'status'      => 1,
                'created_by'  => 1,
                'updated_by'  => 1,
                'created_at'  => Carbon\Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at'  => Carbon\Carbon::now()->format('Y-m-d H:i:s')
            ], [
                'id'          => 2,
                'uuid'        => getUUID(),
                'avatar'      => NULL,
                'name'        => 'Rizky Rinaldi',
                'username'    => 'rizky',
                'email'       => 'rizky@varbsdigital.com',
                'password'    => bcrypt('secret'),
                'birth_place' => NULL,
                'birth_date'  => NULL,
                'gender'      => 'male',
                'phone'       => NULL,
                'regency_id'  => NULL,
                'isAdmin'     => 1,
                'status'      => 1,
                'created_by'  => 1,
                'updated_by'  => 1,
                'created_at'  => Carbon\Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at'  => Carbon\Carbon::now()->format('Y-m-d H:i:s')
            ], [
                'id'          => 3,
                'uuid'        => getUUID(),
                'avatar'      => NULL,
                'name'        => 'Admin',
                'username'    => 'admin',
                'email'       => 'admin@aspirasi.co.id',
                'password'    => bcrypt('secret'),
                'birth_place' => NULL,
                'birth_date'  => NULL,
                'gender'      => NULL,
                'phone'       => NULL,
                'regency_id'  => NULL,
                'isAdmin'     => 1,
                'status'      => 1,
                'created_by'  => 1,
                'updated_by'  => 1,
                'created_at'  => Carbon\Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at'  => Carbon\Carbon::now()->format('Y-m-d H:i:s')
            ], [
                'id'          => 4,
                'uuid'        => getUUID(),
                'avatar'      => NULL,
                'name'        => 'Contributor',
                'username'    => 'contributor',
                'email'       => 'contributor@aspirasi.co.id',
                'password'    => bcrypt('secret'),
                'birth_place' => NULL,
                'birth_date'  => NULL,
                'gender'      => NULL,
                'phone'       => NULL,
                'regency_id'  => NULL,
                'isAdmin'     => 1,
                'status'      => 1,
                'created_by'  => 1,
                'updated_by'  => 1,
                'created_at'  => Carbon\Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at'  => Carbon\Carbon::now()->format('Y-m-d H:i:s')
            ], [
                'id'          => 5,
                'uuid'        => getUUID(),
                'avatar'      => NULL,
                'name'        => 'Editor',
                'username'    => 'editor',
                'email'       => 'editor@aspirasi.co.id',
                'password'    => bcrypt('secret'),
                'birth_place' => NULL,
                'birth_date'  => NULL,
                'gender'      => NULL,
                'phone'       => NULL,
                'regency_id'  => NULL,
                'isAdmin'     => 1,
                'status'      => 1,
                'created_by'  => 1,
                'updated_by'  => 1,
                'created_at'  => Carbon\Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at'  => Carbon\Carbon::now()->format('Y-m-d H:i:s')
            ]
        ]);
    }
}
