<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Election extends Model
{
    protected $fillable = [
        'uuid', 'slug', 'title', 'description', 'end_date', 'status', 'created_by', 'updated_by'
    ];

    public function figures()
    {
        return $this->belongsToMany(
            Figure::class,
            'election_figure',
            'election_id',
            'figure_id'
        );
    }
}
