<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0;');

        App\User::truncate();
        $this->call(UsersTableSeeder::class);

        App\Role::truncate();
        DB::unprepared(file_get_contents('database/seeds/roles.sql'));
        $this->command->info('roles.sql seeded!');
        DB::unprepared(file_get_contents('database/seeds/role_user.sql'));
        $this->command->info('role_user.sql seeded!');

        DB::unprepared(file_get_contents('database/seeds/indonesia.sql'));
        $this->command->info('indonesia.sql seeded!');

        DB::statement('SET FOREIGN_KEY_CHECKS = 1;');
    }
}
