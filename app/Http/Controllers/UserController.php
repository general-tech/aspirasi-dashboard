<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');

        $this->data['module'] = 'User';
        $this->data['breadcrumbs'] = getBreadcrumb();
    }

    public function index()
    {
        $this->data['title'] = 'Daftar ' . $this->data['module'];
        $this->data['users'] = (
            User::where('status', '&', 3)
                ->where('isAdmin', 2)
                ->orderBy('created_at', 'DESC')
                ->paginate(10)
        );

        return view('pages.user.user-index', $this->data);
    }

    public function update(Request $request)
    {
        User::where('uuid', $request->uuid)
            ->firstOrFail()
            ->update([
                'status' => 1
            ]);
    }
}
