<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogsTable extends Migration
{
    public function up()
    {
        Schema::create('logs', function (Blueprint $table) {
            $table->increments('id');
            $table->char('uuid', 36)->unique();
            $table->integer('user_id')->unsigned();
            $table->integer('category_id')->unsigned();
            $table->tinyInteger('category_type')->comment('1: Figure, 2: Policy');
            $table->tinyInteger('option')->default(1)->comment('1: Agree / Choose, 2: Disagree');
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    public function down()
    {
        Schema::dropIfExists('logs');
    }
}
