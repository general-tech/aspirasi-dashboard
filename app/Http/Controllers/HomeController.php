<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');

        $this->data['module'] = 'Home';
        $this->data['breadcrumbs'] = getBreadcrumb();
    }

    public function index()
    {
        $this->data['title'] = $this->data['module'];

        // return view('home', $this->data);
        return redirect()
            ->route('profil');
    }
}
