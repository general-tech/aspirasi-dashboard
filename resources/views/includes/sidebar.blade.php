<div class="page-sidebar-wrapper">
    <div class="page-sidebar navbar-collapse collapse">
        <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
            <li class="sidebar-toggler-wrapper hide">
                <div class="sidebar-toggler">
                    <span></span>
                </div>
            </li>
            {{-- <li class="nav-item start{{ setActive(['home']) }}">
                <a href="{{ route('home') }}" class="nav-link">
                    <i class="fa fa-home"></i>
                    <span class="title">Home</span>
                    <span class="selected"></span>
                </a>
            </li> --}}
            <li class="heading">
                <h3 class="uppercase">Content</h3>
            </li>
            <li class="nav-item{{ setActive(['berita']) }}">
                <a href="{{ route('berita') }}" class="nav-link">
                    <i class="fa fa-newspaper-o"></i>
                    <span class="title">Berita</span>
                    <span class="selected"></span>
                </a>
            </li>
            <li class="nav-item{{ setActive(['kebijakan']) }}">
                <a href="{{ route('kebijakan') }}" class="nav-link">
                    <i class="fa fa-commenting-o"></i>
                    <span class="title">Kebijakan</span>
                    <span class="selected"></span>
                </a>
            </li>
            <li class="nav-item{{ setActive(['pemilihan', 'tokoh']) }}">
                <a class="nav-link nav-toggle">
                    <i class="fa fa-bar-chart"></i>
                    <span class="title">Pemilihan</span>
                    <span class="selected"></span>
                    <span class="arrow{{ setActive(['pemilihan', 'tokoh']) }}"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item{{ setActive(['pemilihan']) }}">
                        <a href="{{ route('pemilihan') }}" class="nav-link">
                            <span class="title">Pemilihan</span>
                        </a>
                    </li>
                    <li class="nav-item{{ setActive(['tokoh']) }}">
                        <a href="{{ route('tokoh') }}" class="nav-link">
                            <span class="title">Tokoh</span>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="heading">
                <h3 class="uppercase">Configure</h3>
            </li>
            <li class="nav-item{{ setActive(['user']) }}">
                <a href="{{ route('user') }}" class="nav-link">
                    <i class="fa fa-users"></i>
                    <span class="title">User</span>
                    <span class="selected"></span>
                </a>
            </li>
            <li class="nav-item{{ setActive(['profil']) }}">
                <a href="{{ route('profil') }}" class="nav-link">
                    <i class="fa fa-user"></i>
                    <span class="title">Profil Saya</span>
                    <span class="selected"></span>
                </a>
            </li>
            @if (Auth::user()->hasRole(['admin']))
                <li class="nav-item{{ setActive(['admin']) }}">
                    <a href="{{ route('admin') }}" class="nav-link">
                        <i class="fa fa-user-secret"></i>
                        <span class="title">Admin</span>
                        <span class="selected"></span>
                    </a>
                </li>
            @endif
            <br>
            <li class="nav-item">
                <a href="{{ route('logout') }}" class="nav-link"
                    onclick="event.preventDefault();
                             document.getElementById('sidebar-logout-form').submit();">
                    <i class="fa fa-power-off"></i>
                    <span class="title">Log Out</span>
                </a>

                <form id="sidebar-logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            </li>
        </ul>
    </div>
</div>
