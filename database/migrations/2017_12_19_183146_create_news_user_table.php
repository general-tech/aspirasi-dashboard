<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsUserTable extends Migration
{
    public function up()
    {
        Schema::create('news_user', function (Blueprint $table) {
            $table->integer('news_id')->unsigned();
            $table->integer('user_id')->unsigned();

            $table->foreign('news_id')->references('id')->on('news')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->primary(['news_id', 'user_id']);
        });
    }

    public function down()
    {
        Schema::dropIfExists('news_user');
    }
}
