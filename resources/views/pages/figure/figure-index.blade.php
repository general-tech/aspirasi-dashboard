@extends ('layouts.app')

@section ('content')
    @if (session('status'))
        <div class="note note-{{ session('status') }}">
            <p>{{ session('text') }}</p>
        </div>
        <script type="text/javascript">
            setTimeout(function() {
                $('div.note').slideUp();
            }, 3000);
        </script>
    @endif

    <div class="portlet box grey-gallery">
        <div class="portlet-title">
            <div class="caption">{{ $title }}</div>
            <div class="actions">
                <a href="{{ route('tokoh.create') }}" class="btn btn-lg green-meadow">
                    Tambah Baru <i class="fa fa-plus"></i>
                </a>
            </div>
        </div>
        <div class="portlet-body">
            <div class="table-responsive">
                <table class="table table-bordered table-striped table-hover">
                    <thead>
                        <tr>
                            <th width="50">#</th>
                            <th>Nama</th>
                            <th width="80" class="text-center">Status</th>
                            @if (Auth::user()->hasRole(['admin', 'editor']))
                                <th width="50"></th>
                            @endif
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($figures as $key => $figure)
                            <tr>
                                <td>{{ ++$key + (($figures->currentPage() - 1) * $figures->perPage()) }}</td>
                                <td><a href="{{ route('tokoh.show', ['uuid' => $figure->uuid]) }}">{{ $figure->name }}</a></td>
                                <td align="middle">
                                    @if ($figure->status == 1)
                                        <span class="label label bg-blue">Terbit</span>
                                    @elseif ($figure->status == 2)
                                        <span class="label label bg-yellow-casablanca">Draf</span>
                                    @endif
                                </td>
                                @if (Auth::user()->hasRole(['admin', 'editor']))
                                    <td align="middle">
                                        <span class="font-lg font-red" role="button" data-id="{{ $figure->uuid }}" data-module="{{ $module }}"
                                            onclick="event.preventDefault()
                                                trash(this)">
                                            <i class="fa fa-trash"></i>
                                        </span>
                                    </td>
                                @endif
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            {{ $figures->links() }}
        </div>
    </div>
@endsection
