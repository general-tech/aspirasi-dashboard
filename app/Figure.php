<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Figure extends Model
{
    protected $fillable = [
        'uuid', 'slug', 'name', 'description', 'status', 'created_by', 'updated_by'
    ];
}
