<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

use App\User;

use Auth;
use Lang;
use Validator;

class LoginController extends Controller
{
    use AuthenticatesUsers;

    protected $redirectTo = 'profil';

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function username()
    {
        return 'username';
    }

    protected function sendFailedLoginResponse(Request $request)
    {
        if (!User::where('username', $request->username)->first()) {
            return redirect()->back()
                ->withInput($request->only($this->username(), 'remember'))
                ->withErrors([
                    $this->username() => Lang::get('auth.username'),
                ]);
        }

        if (!User::where('username', $request->username)->where('password', bcrypt($request->password))->first()) {
            return redirect()->back()
                ->withInput($request->only($this->username(), 'remember'))
                ->withErrors([
                    'password' => Lang::get('auth.password'),
                ]);
        }
    }

    protected function authenticated(Request $request, $user)
    {
        $messages = [
            'isAdmin.in' => 'These credentials do not match our records.',
            'status.in'  => 'Your account has not been activated.'
        ];

        $validator = Validator::make($user['attributes'], [
            'isAdmin' => [
                Rule::in([1]),
            ],
            'status'  => [
                Rule::in([1, 2]),
            ]
        ], $messages);


        if ($validator->fails()) {
            Auth::logout();

            return redirect()
                ->route('login')
                ->withInput($request->only($this->username(), 'remember'))
                ->withErrors($validator);
        } else {
            return redirect()
                ->route('profil');
        }
    }
}
