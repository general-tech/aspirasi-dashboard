<?php

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');

Route::group(['prefix' => 'berita'], function () {
    Route::get('/', 'NewsController@index')->name('berita');
    Route::get('/tambah', 'NewsController@create')->name('berita.create');
    Route::post('/store', 'NewsController@store')->name('berita.store');
    Route::get('/detail', 'NewsController@show')->name('berita.show');
    Route::get('/ubah', 'NewsController@edit')->name('berita.edit');
    Route::patch('/update', 'NewsController@update')->name('berita.update');
    Route::patch('/destroy', 'NewsController@destroy')->name('berita.destroy');
});

Route::group(['prefix' => 'kebijakan'], function () {
    Route::get('/', 'PolicyController@index')->name('kebijakan');
    Route::get('/tambah', 'PolicyController@create')->name('kebijakan.create');
    Route::post('/store', 'PolicyController@store')->name('kebijakan.store');
    Route::get('/detail', 'PolicyController@show')->name('kebijakan.show');
    Route::get('/ubah', 'PolicyController@edit')->name('kebijakan.edit');
    Route::patch('/update', 'PolicyController@update')->name('kebijakan.update');
    Route::patch('/destroy', 'PolicyController@destroy')->name('kebijakan.destroy');
});

Route::group(['prefix' => 'pemilihan'], function () {
    Route::get('/', 'ElectionController@index')->name('pemilihan');
    Route::get('/tambah', 'ElectionController@create')->name('pemilihan.create');
    Route::post('/store', 'ElectionController@store')->name('pemilihan.store');
    Route::get('/detail', 'ElectionController@show')->name('pemilihan.show');
    Route::get('/ubah', 'ElectionController@edit')->name('pemilihan.edit');
    Route::patch('/update', 'ElectionController@update')->name('pemilihan.update');
    Route::patch('/destroy', 'ElectionController@destroy')->name('pemilihan.destroy');
});

Route::group(['prefix' => 'tokoh'], function () {
    Route::get('/', 'FigureController@index')->name('tokoh');
    Route::get('/tambah', 'FigureController@create')->name('tokoh.create');
    Route::post('/store', 'FigureController@store')->name('tokoh.store');
    Route::get('/detail', 'FigureController@show')->name('tokoh.show');
    Route::get('/ubah', 'FigureController@edit')->name('tokoh.edit');
    Route::patch('/update', 'FigureController@update')->name('tokoh.update');
    Route::patch('/destroy', 'FigureController@destroy')->name('tokoh.destroy');
});

Route::group(['prefix' => 'user'], function () {
    Route::get('/', 'UserController@index')->name('user');
    Route::get('/detail', 'UserController@show')->name('user.show');
    Route::get('/ubah', 'UserController@edit')->name('user.edit');
    Route::patch('/update', 'UserController@update')->name('user.update');
});

Route::group(['prefix' => 'admin'], function () {
    Route::get('/', 'AdminController@index')->name('admin');
    Route::get('/tambah', 'AdminController@create')->name('admin.create');
    Route::post('/store', 'AdminController@store')->name('admin.store');
    Route::get('/detail', 'AdminController@show')->name('admin.show');
    Route::get('/ubah', 'AdminController@edit')->name('admin.edit');
    Route::patch('/update', 'AdminController@update')->name('admin.update');
    Route::patch('/destroy', 'AdminController@destroy')->name('admin.destroy');
});

Route::group(['prefix' => 'profil'], function () {
    Route::get('/', 'ProfileController@index')->name('profil');
    Route::get('/ubah', 'ProfileController@edit')->name('profil.edit');
    Route::patch('/update', 'ProfileController@update')->name('profil.update');
});
