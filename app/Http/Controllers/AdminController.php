<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Role;
use App\User as Admin;

use Auth;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');

        $this->data['module'] = 'Admin';
        $this->data['breadcrumbs'] = getBreadcrumb();
    }

    public function index()
    {
        $this->data['title'] = 'Daftar ' . $this->data['module'];
        $this->data['admins'] = (
            Admin::where('isAdmin', 1)
                ->where('status', '&', 3)
                ->orderBy('created_at', 'ASC')
                ->paginate(10)
        );
        $this->data['roles'] = Role::get();

        return view('pages.admin.admin-index', $this->data);
    }

    public function create()
    {
        $this->data['title'] = 'Tambah ' . $this->data['module'];
        $this->data['method'] = 'POST';
        $this->data['admin'] = new Admin;
        $this->data['roles'] = Role::get();

        return view('pages.admin.admin-form', $this->data);
    }

    public function store(Request $request)
    {
        $messages = [
            'name.required'     => 'Nama dibutuhkan',
            'username.required' => 'Username dibutuhkan',
            'email.required'    => 'Email dibutuhkan',
            'role.required'     => 'Role dibutuhkan',
            'password.required' => 'Password dibutuhkan'
        ];

        $this->validate($request, [
            'name'     => 'required',
            'username' => 'required|alpha_num|min:5|unique:users',
            'email'    => 'required|email|unique:users',
            'role'     => 'required',
            'password' => 'required|min:6|confirmed'
        ], $messages);

        $request['uuid'] = getUUID();
        $request['password'] = bcrypt($request->password);
        $request['isAdmin'] = 1;
        $request['created_by'] = Auth::id();
        $request['updated_by'] = Auth::id();

        $admin = Admin::create($request->all());
        $admin->roles()->sync([$request->role]);

        return redirect()
            ->route('admin')
            ->with('status', 'success')
            ->with('text', 'Berhasil menambah ' . $this->data['module'] . ' baru');
    }

    public function show(Request $request)
    {
        $this->data['title'] = 'Lihat ' . $this->data['module'];
        $this->data['method'] = 'PATCH';
        $this->data['admin'] = (
            Admin::where('uuid', $request->uuid)
                ->where('status', '&', 3)
                ->firstOrFail()
        );
        $this->data['roles'] = Role::get();

        return view('pages.admin.admin-form', $this->data);
    }

    public function edit(Request $request)
    {
        $this->data['title'] = 'Edit ' . $this->data['module'];
        $this->data['method'] = 'PATCH';
        $this->data['admin'] = (
            Admin::where('uuid', $request->uuid)
                ->where('status', '&', 3)
                ->firstOrFail()
        );
        $this->data['roles'] = Role::get();

        return view('pages.admin.admin-form', $this->data);
    }

    public function update(Request $request)
    {
        $messages = [
            'name.required'     => 'Nama dibutuhkan',
            'username.required' => 'Username dibutuhkan',
            'email.required'    => 'Email dibutuhkan',
            'role.required'     => 'Role dibutuhkan',
            'password.required' => 'Password dibutuhkan'
        ];

        $this->validate($request, [
            'name'     => 'required',
            'username' => 'required|alpha_num|min:5|unique:users,username,' . $request->uuid . ',uuid',
            'email'    => 'required|email|unique:users,email,' . $request->uuid . ',uuid',
            'role'     => 'required',
            'password' => 'sometimes|nullable|min:6|confirmed'
        ], $messages);

        $request['password'] = bcrypt($request->password);
        $request['updated_by'] = Auth::id();

        $admin = Admin::where('uuid', $request->uuid)
            ->firstOrFail();
        $admin->update($request->all());
        $admin->roles()->sync([$request->role]);

        return redirect()
            ->route('admin')
             ->with('status', 'success')
             ->with('text', 'Berhasil mengubah ' . $this->data['module']);
    }

    public function destroy(Request $request)
    {
        $admin = Admin::where('uuid', $request->uuid)->first();

        if ($admin === null) {
            $request->session()->flash('status', 'danger');
            $request->session()->flash('text', 'Gagal untuk menghapus ' . $this->data['module']);
        } else if ($admin->uuid === Auth::user()->uuid) {
            $request->session()->flash('status', 'danger');
            $request->session()->flash('text', 'Tidak dapat menghapus akun Anda sendiri');
        } else {
            $admin->update([
                'status'     => 4,
                'updated_by' => Auth::id()
            ]);

            $request->session()->flash('status', 'success');
            $request->session()->flash('text', 'Berhasil menghapus ' . $this->data['module']);
        }
    }
}
