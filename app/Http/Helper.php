<?php

if (!function_exists('setActive')) {
    function setActive($routes, $class = ' active open') {
        $count = 0;

        foreach ($routes as $key => $route) {
            $count += substr(Route::currentRouteName(), 0, strlen($route)) === $route;
        }

        if ($count > 0) {
            return $class;
        } else {
            return '';
        }
    }
}

if (!function_exists('checkRoute')) {
    function checkRoute($route) {
        return substr(Route::currentRouteName(), 0, strlen($route)) === $route;
    }
}

if (!function_exists('checkSubRoute')) {
    function checkSubRoute($route) {
        return substr(Route::currentRouteName(), -strlen($route)) === $route;
    }
}

if (!function_exists('getBreadcrumb')) {
    function getBreadcrumb() {
        $path = Request::path();

        if ($path == '/') {
            $breadcrumb = [];
        } else {
            $segments = explode('/', $path);

            foreach ($segments as $key => $segment) {
                // if (strpos($segment[$key], '-')) {
                //     $segment[$key] = explode('-', $segment[$key]);
                //     $segment[$key] = implode(' ', $segment[$key]);
                // }

                if ($segment == $segments[0]) {
                    $breadcrumb[] = [
                        'route' => $segments[$key],
                        'segment' => ucwords($segments[$key])
                    ];
                } else {
                    $breadcrumb[] = [
                        'route' => $segments[$key - 1] . '.' . $segments[$key],
                        'segment' => ucwords($segments[$key])
                    ];
                }
            }
        }

        return $breadcrumb;
    }
}

if (!function_exists('getUUID')) {
    function getUUID() {
        return DB::SELECT('SELECT UUID() AS UUID')[0]->UUID;
    }
}
