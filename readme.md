# Aspirasi Dashboard

## Getting Started
### Prerequisites
* PHP >= 5.6.4
* OpenSSL PHP Extension
* PDO PHP Extension
* Mbstring PHP Extension
* Tokenizer PHP Extension
* XML PHP Extension

### Installing
Configure .env file, then
```
composer update
```

## Built With
* [Laravel](https://laravel.com/docs/5.4/) - The PHP Framework For Web Artisans | Version 5.4