<?php

namespace App;

use Zizaco\Entrust\Traits\EntrustUserTrait;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use EntrustUserTrait;
    use Notifiable;

    protected $fillable = [
        'uuid', 'name', 'username', 'email', 'password', 'isAdmin', 'status', 'created_by', 'updated_by'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];
}
