<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>Login</title>

        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css">
        <link href="{{ asset('assets/global/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('assets/global/css/components.min.css') }}" rel="stylesheet" id="style_components" type="text/css">
        <link href="{{ asset('assets/pages/css/login.min.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css">
        <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">
    </head>

    <body class=" login">
        <div class="logo">
            <img src="{{ asset('img/logo-type.png') }}" alt="" width="200">
        </div>
        <div class="content">
            <form class="login-form" method="POST" action="{{ route('login') }}">
                {{ csrf_field() }}

                <h3 class="form-title font-green">Sign In</h3>

                @if ($errors->has('status'))
                    <div class="form-group error">
                        <small><span class="help-block text-danger text-center">{{ $errors->first('status') }}</span></small>
                    </div>
                    <script type="text/javascript">
                        setTimeout(function() {
                            $('div.error').slideUp();
                        }, 3000);
                    </script>
                @endif

                <div class="form-group{{ ($errors->has('username') || $errors->has('isAdmin')) ? ' has-error' : '' }}">
                    <label class="control-label visible-ie8 visible-ie9">Username</label>
                    <input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off" placeholder="Username" name="username" value="{{ old('username') }}" required autofocus>
                    @if ($errors->has('username') || $errors->has('isAdmin'))
                        <small><span class="help-block text-danger">* {{ $errors->first('username') . $errors->first('isAdmin') }}</span></small>
                    @endif
                </div>
                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <label class="control-label visible-ie8 visible-ie9">Password</label>
                    <input class="form-control form-control-solid placeholder-no-fix" type="password" autocomplete="off" placeholder="Password" name="password" required>
                    @if ($errors->has('password'))
                        <small><span class="help-block text-danger">* {{ $errors->first('password') }}</span></small>
                    @endif
                </div>
                <div class="form-group">
                    <button type="submit" class="btn green uppercase">Login</button>
                    <label class="rememberme check mt-checkbox mt-checkbox-outline">
                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>Remember
                        <span></span>
                    </label>
                </div>
            </form>
        </div>
        <div class="copyright">2018 &copy; Aspirasi</div>
        <!--[if lt IE 9]>
<script src="{{ asset('assets/global/plugins/respond.min.js') }}"></script>
<script src="{{ asset('assets/global/plugins/excanvas.min.js') }}"></script>
<script src="{{ asset('assets/global/plugins/ie8.fix.min.js') }}"></script>
<![endif]-->
        <script src="{{ asset('assets/global/plugins/jquery.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/global/plugins/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
    </body>

</html>
