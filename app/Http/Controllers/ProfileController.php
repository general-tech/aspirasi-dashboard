<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User as Admin;

use Auth;

class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');

        $this->data['module'] = 'Profil Saya';
        $this->data['breadcrumbs'] = getBreadcrumb();
    }

    public function index()
    {
        $this->data['title'] = 'Detail ' . $this->data['module'];
        $this->data['method'] = 'PATCH';
        $this->data['admin'] = (
            Admin::where('uuid', Auth::user()->uuid)
                ->where('status', '&', 3)
                ->firstOrFail()
        );

        return view('pages.profile.profile-form', $this->data);
    }

    public function edit()
    {
        $this->data['title'] = 'Ubah ' . $this->data['module'];
        $this->data['method'] = 'PATCH';
        $this->data['admin'] = (
            Admin::where('uuid', Auth::user()->uuid)
                ->where('status', '&', 3)
                ->firstOrFail()
        );

        return view('pages.profile.profile-form', $this->data);
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'name'     => 'required',
            'username' => 'required|alpha_num|min:5|unique:users,username,' . Auth::user()->uuid . ',uuid',
            'email'    => 'required|email|unique:users,email,' . Auth::user()->uuid . ',uuid',
            'password' => 'sometimes|nullable|min:6|confirmed'
        ]);

        $request['password'] = bcrypt($request->password);
        $request['updated_by'] = Auth::id();

        Admin::where('uuid', Auth::user()->uuid)
            ->firstOrFail()
            ->update($request->all());

        return redirect()
            ->route('profil')
             ->with('status', 'success')
             ->with('text', 'Berhasil mengubah ' . $this->data['module']);
    }
}
