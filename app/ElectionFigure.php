<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ElectionFigure extends Model
{
    protected $table = 'election_figure';
}
