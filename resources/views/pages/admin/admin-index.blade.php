@extends('layouts.app')

@section('content')
    @if (session('status'))
        <div class="note note-{{ session('status') }}">
            <p>{{ session('text') }}</p>
        </div>
        <script type="text/javascript">
            setTimeout(function() {
                $('div.note').slideUp();
            }, 3000);
        </script>
    @endif

    <div class="portlet box grey-gallery">
        <div class="portlet-title">
            <div class="caption">{{ $title }}</div>
            <div class="actions">
                <a href="{{ route('admin.create') }}" class="btn btn-lg green-meadow">
                    Tambah Baru <i class="fa fa-plus"></i>
                </a>
            </div>
        </div>
        <div class="portlet-body">
            <div class="table-responsive">
                <table class="table table-bordered table-striped table-hover">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Username</th>
                            <th>Email</th>
                            <th>Role</th>
                            <th width="100" class="text-center">Status</th>
                            <th width="50"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($admins as $key => $admin)
                            <tr>
                                <td>{{ ++$key + (($admins->currentPage() - 1) * $admins->perPage()) }}</td>
                                <td><a href="{{ route('admin.show', ['uuid' => $admin->uuid]) }}">{{ $admin->name }}</a></td>
                                <td>{{ $admin->username }}</td>
                                <td>{{ $admin->email }}</td>
                                <td>{{ ucfirst($admin->roles->first()->name) }}</td>
                                <td align="middle">
                                    @if ($admin->status & 3)
                                        <span class="label label-success font-default bold">Active</span>
                                    @elseif ($admin->status == 4)
                                        <span class="label label-danger font-default bold">Inactive</span>
                                    @endif
                                </td>
                                <td align="middle">
                                    <span class="font-lg font-red" role="button" data-id="{{ $admin->uuid }}" data-module="{{ $module }}"
                                        onclick="event.preventDefault()
                                            trash(this)">
                                        <i class="fa fa-trash"></i>
                                    </span>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            {{ $admins->links() }}
        </div>
    </div>
@endsection
