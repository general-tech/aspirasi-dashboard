<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePolicyUserTable extends Migration
{
    public function up()
    {
        Schema::create('policy_user', function (Blueprint $table) {
            $table->integer('policy_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->tinyInteger('option')->unsigned()->comment('1: Agree, 2: Disagree');

            $table->foreign('policy_id')->references('id')->on('policies')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->primary(['policy_id', 'user_id']);
            $table->index('option');
        });
    }

    public function down()
    {
        Schema::dropIfExists('policy_user');
    }
}
