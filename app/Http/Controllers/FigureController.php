<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Figure;

use Auth;
use Cloudder;
use Entrust;
use File;

class FigureController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');

        $this->data['module'] = 'Tokoh';
        $this->data['breadcrumbs'] = getBreadcrumb();
    }

    public function index()
    {
        $this->data['title'] = 'Daftar ' . $this->data['module'];
        $this->data['figures'] = (
            Figure::where('status', '&', 3)
                ->orderBy('created_at', 'DESC')
                ->paginate(10)
        );

        return view('pages.figure.figure-index', $this->data);
    }

    public function create()
    {
        $this->data['title'] = 'Tambah ' . $this->data['module'];
        $this->data['method'] = 'POST';
        $this->data['figure'] = new Figure;

        return view('pages.figure.figure-form', $this->data);
    }

    public function store(Request $request)
    {
        $messages = [
            'name.required'        => 'Nama dibutuhkan',
            'description.required' => 'Deskripsi dibutuhkan',
            'thumbnail.required'   => 'Gambar dibutuhkan'
        ];

        $this->validate($request, [
            'name'        => 'required',
            'description' => 'required',
            'thumbnail'   => 'required|image|mimes:jpeg,png|file|max:1024'
        ], $messages);

        $request['uuid'] = getUUID();

        if ($request->file('thumbnail')) {
            Cloudder::upload(
                $request->file('thumbnail'),
                $publicId = 'tokoh/' . $request['uuid'],
                $option = array(
                    "transformation" => array(
                        array(
                            "width"  => 500,
                            "height" => 500,
                            "crop"   => "fill"
                        )
                    )
                )
            );
        }

        if ($request->description) {
            $dom = new \DomDocument();
            libxml_use_internal_errors(true);
            $dom->loadHTML($request->description, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
            libxml_clear_errors();
            $images = $dom->getElementsByTagName('img');

            foreach($images as $k => $img) {
                $data = $img->getAttribute('src');

                if (count(explode(';', $data)) > 1) {
                    list($type, $data) = explode(';', $data);
                    list(, $data) = explode(',', $data);

                    $data = base64_decode($data);
                    $tmp_name = '/tmp/' . time() . $k . '.jpg';
                    $path = public_path('/img') . $tmp_name;

                    file_put_contents($path, $data);

                    Cloudder::upload(
                        $path,
                        $publicId = ''
                    );

                    $img_id = Cloudder::getResult()['public_id'];

                    $img->removeAttribute('data-id');
                    $img->setAttribute('data-id', $img_id);
                    $img_url = Cloudder::show($img_id);

                    File::delete($path);
                } else {
                    $img_url = $data;
                }

                $img->removeAttribute('data-filename');
                $img->removeAttribute('style');
                $img->removeAttribute('src');
                $img->setAttribute('src', $img_url);
            }

            $description = $dom->saveHTML();

            $request['description'] = $description;
        }

        $request['created_by'] = Auth::id();
        $request['updated_by'] = Auth::id();

        Figure::create($request->all());

        return redirect()
            ->route('tokoh')
            ->with('status', 'success')
            ->with('text', 'Berhasil menambah ' . $this->data['module'] . ' baru');
    }

    public function show(Request $request)
    {
        $this->data['title'] = 'Detail ' . $this->data['module'];
        $this->data['method'] = 'PATCH';
        $this->data['figure'] = (
            Figure::where('uuid', $request->uuid)
                ->where('status', '&', 3)
                ->firstOrFail()
        );

        return view('pages.figure.figure-form', $this->data);
    }

    public function edit(Request $request)
    {
        $this->data['title'] = 'Ubah ' . $this->data['module'];
        $this->data['method'] = 'PATCH';
        $this->data['figure'] = (
            Figure::where('uuid', $request->uuid)
                ->where('status', '&', 3)
                ->firstOrFail()
        );

        return view('pages.figure.figure-form', $this->data);
    }

    public function update(Request $request)
    {
        $messages = [
            'name.required'        => 'Nama dibutuhkan',
            'description.required' => 'Deskripsi dibutuhkan',
            'thumbnail.required'   => 'Gambar dibutuhkan'
        ];

        $this->validate($request, [
            'name'        => 'required',
            'description' => 'required',
            'thumbnail'   => 'sometimes|nullable|image|mimes:jpeg,png|file|max:1024'
        ]);

        if ($request->file('thumbnail')) {
            Cloudder::delete('tokoh/' . $request->uuid);
            Cloudder::upload(
                $request->file('thumbnail'),
                $publicId = 'tokoh/' . $request->uuid,
                $option = array(
                    "transformation" => array(
                        array(
                            "width"  => 500,
                            "height" => 500,
                            "crop"   => "fill"
                        )
                    )
                )
            );
        }

        if ($request->description) {
            $dom = new \DomDocument();
            libxml_use_internal_errors(true);
            $dom->loadHTML($request->description, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
            libxml_clear_errors();
            $images = $dom->getElementsByTagName('img');

            foreach($images as $k => $img) {
                $data = $img->getAttribute('src');

                if (count(explode(';', $data)) > 1) {
                    list($type, $data) = explode(';', $data);
                    list(, $data) = explode(',', $data);

                    $data = base64_decode($data);
                    $tmp_name = '/tmp/' . time() . $k . '.jpg';
                    $path = public_path('/img') . $tmp_name;

                    file_put_contents($path, $data);

                    Cloudder::upload(
                        $path,
                        $publicId = ''
                    );

                    $img_id = Cloudder::getResult()['public_id'];

                    $img->removeAttribute('data-id');
                    $img->setAttribute('data-id', $img_id);
                    $img_url = Cloudder::show($img_id);

                    File::delete($path);
                } else {
                    $img_url = $data;
                }

                $img->removeAttribute('data-filename');
                $img->removeAttribute('style');
                $img->removeAttribute('src');
                $img->setAttribute('src', $img_url);
            }

            $description = $dom->saveHTML();

            $request['description'] = $description;
        }

        $request['updated_by'] = Auth::id();

        Figure::where('uuid', $request->uuid)
            ->firstOrFail()
            ->update($request->all());

        return redirect()
            ->route('tokoh')
            ->with('status', 'success')
            ->with('text', 'Berhasil mengubah ' . $this->data['module']);
    }

    public function destroy(Request $request)
    {
        $figure = Figure::where('uuid', $request->uuid)->first();

        if ($figure === null) {
            $request->session()->flash('status', 'danger');
            $request->session()->flash('text', 'Gagal untuk menghapus ' . $this->data['module']);
        } else {
            $figure->update([
                'status'     => 4,
                'updated_by' => Auth::id()
            ]);

            $request->session()->flash('status', 'success');
            $request->session()->flash('text', 'Berhasil menghapus ' . $this->data['module']);
        }
    }
}
