@extends ('layouts.app')

@section ('content')
    <div class="portlet box grey-gallery">
        <div class="portlet-title">
            <div class="caption">
                {{ $title }}
                @if ($election->status == 1)
                    <span class="label bg-blue">Terbit</span>
                @elseif ($election->status == 2)
                    <span class="label bg-yellow-casablanca">Draf</span>
                @endif
            </div>
            <div class="actions">
                @if (checkSubRoute('show'))
                    <a href="{{ route('pemilihan.edit', ['uuid' => $election->uuid]) }}" class="btn btn-lg green-jungle">
                        Ubah <i class="fa fa-edit"></i>
                    </a>
                @endif
            </div>
        </div>
        <div class="portlet-body">
            <form id="form" class="form-horizontal form-bordered" method="POST" action="{{ $election->uuid ? route('pemilihan.update', ['uuid' => $election->uuid]) : route('pemilihan.store') }}" enctype="multipart/form-data">
                {{ method_field($method) }}
                {{ csrf_field() }}

                <div class="form-body">
                    <div class="form-group{{ $errors->has('thumbnail') ? ' has-error' : '' }}">
                        <label class="control-label col-md-3">Gambar</label>
                        <div class="col-md-9">
                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                <div class="fileinput-new thumbnail" style="max-width: 200px; max-height: 150px;">
                                    <img src="{{ $election->uuid ? Cloudder::show('pemilihan/' . $election->uuid) : 'http://www.placehold.it/200x150/AAAAAA/333333&text=no+image' }}">
                                </div>
                                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div>
                                @if (!checkSubRoute('show'))
                                    <div>
                                        <span class="btn blue-soft btn-file">
                                            <span class="fileinput-new">Pilih Gambar</span>
                                            <span class="fileinput-exists">Ubah</span>
                                            <input type="file" name="thumbnail" accept="image/*" required
                                                @if (!$election->uuid)
                                                    required
                                                    oninvalid="this.setCustomValidity('Please select an image.')"
                                                    onchange="this.setCustomValidity('')"
                                                @endif
                                            >
                                        </span>
                                        <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput">Hapus</a>
                                    </div>
                                @endif
                            </div>
                            @if ($errors->has('thumbnail'))
                                <small><span class="help-block text-danger">* {{ $errors->first('thumbnail') }}</span></small>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                        <label class="col-md-3 control-label">Judul</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" value="{{ old('title') ? : $election->title }}" name="title" autofocus>
                            @if ($errors->has('title'))
                                <small><span class="help-block text-danger">* {{ $errors->first('title') }}</span></small>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('end_date') ? ' has-error' : '' }}">
                        <label class="col-md-3 control-label">Tanggal Berakhir</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control date-picker" value="{{ $election->end_date ? date('d M Y', strtotime($election->end_date)) : old('end_date') ? : date('d M Y') }}" name="end_date" readonly>
                            @if ($errors->has('end_date'))
                                <small><span class="help-block text-danger">* {{ $errors->first('end_date') }}</span></small>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                        <label class="col-md-3 control-label">Deskripsi</label>
                        <div class="col-md-9">
                            <textarea name="description" id="summernote">{{ old('description') ? : $election->description }}</textarea>
                            @if ($errors->has('description'))
                                <small><span class="help-block text-danger">* {{ $errors->first('description') }}</span></small>
                            @endif
                        </div>
                    </div>
                    <h3 class="form-section">Tokoh</h3>
                    @if (count($election->figures))
                        <div class="form-group mt-repeater defaultFigures" style="margin-bottom: 0;">
                            <div class="col-md-12">
                                <div data-repeater-list="defaultFigures">
                                    @foreach ($election->figures as $key => $electionFigure)
                                        @if ($electionFigure->status === 1)
                                            <div data-repeater-item class="mt-repeater-item mt-overflow">
                                                <div class="mt-repeater-cell">
                                                    <select class="form-control select2" name="figure">
                                                        <option></option>
                                                        @foreach ($figures as $key => $figure)
                                                            <option value="{{ $figure->id }}"{{ $electionFigure->id == $figure->id ? ' selected' : '' }}>{{ $figure->name }}</option>
                                                        @endforeach
                                                    </select>
                                                    @if (!checkSubRoute('show'))
                                                        <a href="javascript:;" data-repeater-delete class="btn btn-danger mt-repeater-delete mt-repeater-del-right mt-repeater-btn-inline">
                                                            <i class="fa fa-close"></i>
                                                        </a>
                                                    @endif
                                                </div>
                                            </div>
                                        @endif
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    @endif
                    @if (!checkSubRoute('show'))
                        <div class="form-group mt-repeater setFigures">
                            <div class="col-md-12">
                                <div data-repeater-list="setFigures">
                                    <div data-repeater-item class="mt-repeater-item mt-overflow">
                                        <div class="mt-repeater-cell">
                                            <select class="form-control select2" name="figure">
                                                <option></option>
                                                @foreach ($figures as $key => $figure)
                                                    <option value="{{ $figure->id }}">{{ $figure->name }}</option>
                                                @endforeach
                                            </select>
                                            <a href="javascript:;" data-repeater-delete class="btn btn-danger mt-repeater-delete mt-repeater-del-right mt-repeater-btn-inline">
                                                <i class="fa fa-close"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <a href="javascript:;" data-repeater-create class="btn btn-success mt-repeater-add">
                                    Tambah Tokoh <i class="fa fa-plus"></i>
                                </a>
                            </div>
                        </div>
                    @endif

                    <input type="text" name="status" id="status" class="hidden">
                    @if (!checkSubRoute('show'))
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-12 text-right">
                                    <button type="submit" class="btn yellow-casablanca"
                                        onclick="event.preventDefault();
                                            document.getElementById('status').value = 2;
                                            document.getElementById('form').submit();">
                                        Simpan sebagai Draf <i class="fa fa-file-o"></i>
                                    </button>
                                    @if (Auth::user()->hasRole(['admin', 'editor']))
                                        <button type="submit" class="btn blue"
                                            onclick="event.preventDefault();
                                                document.getElementById('status').value = 1;
                                                document.getElementById('form').submit();">
                                            Terbitkan <i class="fa fa-paper-plane-o"></i>
                                        </button>
                                    @endif
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
            </form>
        </div>
    </div>
@endsection

@section ('script')
    <script type="text/javascript">
        $(document).ready(function() {
            $('#summernote').summernote({
                height: 300
            })

            $.fn.select2.defaults.set('theme', 'bootstrap')

            $("div[data-repeater-list='defaultFigures'] .select2, div[data-repeater-list='setFigures'] .select2").select2({
                placeholder: 'Pilih Tokoh',
                width: null
            })

            @if (checkSubRoute('show'))
                disabledInput()
                $('#summernote').summernote('disable')
            @else
                $('.mt-repeater.defaultFigures').each(function () {
                    $(this).repeater({
                        hide: function (deleteElement) {
                            if (confirm('Are you sure you want to delete this element?')) {
                                $(this).slideUp(deleteElement)
                            }
                        }
                    })
                })

                $('.mt-repeater.setFigures').each(function () {
                    $(this).repeater({
                        show: function () {
                            $(this).slideDown()
                            $("div[data-repeater-list='setFigures'] .select2-container").remove()
                            $("div[data-repeater-list='setFigures'] .select2").select2({
                                placeholder: 'Pilih Tokoh',
                                width: null
                            })
                        },
                        hide: function (deleteElement) {
                            if (confirm('Apakah Anda yakin menghapus Tokoh ini?')) {
                                $(this).slideUp(deleteElement)
                            }
                        }
                    })
                })
            @endif
        })
    </script>
@endsection
