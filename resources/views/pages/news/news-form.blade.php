@extends('layouts.app')

@section('content')
    <div class="portlet box grey-gallery">
        <div class="portlet-title">
            <div class="caption">
                {{ $title }}
                @if ($theNews->status == 1)
                    <span class="label bg-blue">Terbit</span>
                @elseif ($theNews->status == 2)
                    <span class="label bg-yellow-casablanca">Draf</span>
                @endif
            </div>
            <div class="actions">
                @if (checkSubRoute('show'))
                    <a href="{{ route('berita.edit', ['uuid' => $theNews->uuid]) }}" class="btn btn-lg green-jungle">
                        Ubah <i class="fa fa-edit"></i>
                    </a>
                @endif
            </div>
        </div>
        <div class="portlet-body">
            <form id="form" class="form-horizontal form-bordered" method="POST" action="{{ $theNews->uuid ? route('berita.update', ['uuid' => $theNews->uuid]) : route('berita.store') }}" enctype="multipart/form-data">
                {{ method_field($method) }}
                {{ csrf_field() }}

                <div class="form-body">
                    <div class="form-group{{ $errors->has('thumbnail') ? ' has-error' : '' }}">
                        <label class="control-label col-md-3">Gambar</label>
                        <div class="col-md-9">
                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                <div class="fileinput-new thumbnail" style="max-width: 200px; max-height: 150px;">
                                    <img src="{{ $theNews->uuid ? Cloudder::show('berita/' . $theNews->uuid) : 'http://www.placehold.it/200x150/AAAAAA/333333&text=no+image' }}">
                                </div>
                                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div>
                                @if (!checkSubRoute('show'))
                                    <div>
                                        <span class="btn blue-soft btn-file">
                                            <span class="fileinput-new">Pilih Gambar</span>
                                            <span class="fileinput-exists">Ubah</span>
                                            <input type="file" name="thumbnail" accept="image/*" required
                                                @if (!$theNews->uuid)
                                                    required
                                                    oninvalid="this.setCustomValidity('Please select an image.')"
                                                    onchange="this.setCustomValidity('')"
                                                @endif
                                            >
                                        </span>
                                        <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput">Hapus</a>
                                    </div>
                                @endif
                            </div>
                            @if ($errors->has('thumbnail'))
                                <small><span class="help-block text-danger">* {{ $errors->first('thumbnail') }}</span></small>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                        <label class="col-md-3 control-label">Judul</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" value="{{ old('title') ? : $theNews->title }}" name="title" autofocus>
                            @if ($errors->has('title'))
                                <small><span class="help-block text-danger">* {{ $errors->first('title') }}</span></small>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                        <label class="col-md-3 control-label">Deskripsi</label>
                        <div class="col-md-9">
                            <textarea name="description" id="summernote">{{ old('description') ? : $theNews->description }}</textarea>
                            @if ($errors->has('description'))
                                <small><span class="help-block text-danger">* {{ $errors->first('description') }}</span></small>
                            @endif
                        </div>
                    </div>

                    <input type="text" name="status" id="status" class="hidden">
                    @if (!checkSubRoute('show'))
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-12 text-right">
                                    <button type="submit" class="btn yellow-casablanca"
                                        onclick="event.preventDefault();
                                            document.getElementById('status').value = 2;
                                            document.getElementById('form').submit();">
                                        Simpan sebagai Draf <i class="fa fa-file-o"></i>
                                    </button>
                                    @if (Auth::user()->hasRole(['admin', 'editor']))
                                        <button type="submit" class="btn blue"
                                            onclick="event.preventDefault();
                                                document.getElementById('status').value = 1;
                                                document.getElementById('form').submit();">
                                            Terbitkan <i class="fa fa-paper-plane-o"></i>
                                        </button>
                                    @endif
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
            </form>
        </div>
    </div>
@endsection

@section ('script')
    <script type="text/javascript">
        $(document).ready(function() {
            $('#summernote').summernote({
                height: 300
            });

            @if (checkSubRoute('show'))
                disabledInput();
                $('#summernote').summernote('disable');
            @endif
        });
    </script>
@endsection
