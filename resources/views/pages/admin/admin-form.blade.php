@extends('layouts.app')

@section('content')
    <div class="portlet box grey-gallery">
        <div class="portlet-title">
            <div class="caption">
                {{ $title }}
                @if ($admin->status == 1)
                    <span class="label bg-blue">Active</span>
                @elseif ($admin->status == 2)
                    <span class="label bg-yellow-casablanca">Inactive</span>
                @endif
            </div>
            <div class="actions">
                @if (checkSubRoute('show'))
                    <a href="{{ route('admin.edit', ['uuid' => $admin->uuid]) }}" class="btn btn-lg green-jungle">
                        Ubah <i class="fa fa-edit"></i>
                    </a>
                @endif
            </div>
        </div>
        <div class="portlet-body">
            <form id="form" class="form-horizontal form-bordered" method="POST" action="{{ $admin->uuid ? route('admin.update', ['uuid' => $admin->uuid]) : route('admin.store') }}" enctype="multipart/form-data">
                {{ method_field($method) }}
                {{ csrf_field() }}

                <div class="form-body">
                    <div class="form-group">
                        <label class="col-md-3 control-label">Name</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" value="{{ old('name') ? : $admin->name }}" name="name" autofocus>
                            @if ($errors->has('name'))
                                <small><span class="help-block text-danger">* {{ $errors->first('name') }}</span></small>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Username</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" value="{{ old('username') ? : $admin->username }}" name="username">
                            @if ($errors->has('username'))
                                <small><span class="help-block text-danger">* {{ $errors->first('username') }}</span></small>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Email</label>
                        <div class="col-md-9">
                            <input type="email" class="form-control" value="{{ old('email') ? : $admin->email }}" name="email">
                            @if ($errors->has('email'))
                                <small><span class="help-block text-danger">* {{ $errors->first('email') }}</span></small>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Role</label>
                        <div class="col-md-9">
                            <select class="form-control select2" name="role">
                                <option></option>
                                @foreach ($roles as $role)
                                    <option value="{{ $role->id }}"{{ old('role') == $role->id ? ' selected' : (isset($admin->roles->first()->id) ? ($admin->roles->first()->id == $role->id ? ' selected' : '') : '') }}>{{ ucfirst($role->name) }}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('role'))
                                <small><span class="help-block text-danger">* {{ $errors->first('role') }}</span></small>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Password</label>
                        <div class="col-md-9">
                            <input type="password" class="form-control" name="password">
                            @if ($errors->has('password'))
                                <small><span class="help-block text-danger">* {{ $errors->first('password') }}</span></small>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Confirm Password</label>
                        <div class="col-md-9">
                            <input type="password" class="form-control" name="password_confirmation">
                        </div>
                    </div>

                    <input type="text" name="status" id="status" class="hidden">
                    @if (!checkSubRoute('show'))
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-12 text-right">
                                    @if (Auth::user()->hasRole(['admin']))
                                        <button type="submit" class="btn blue"
                                            onclick="event.preventDefault();
                                                document.getElementById('status').value = 1;
                                                document.getElementById('form').submit();">
                                            Simpan <i class="fa fa-paper-plane-o"></i>
                                        </button>
                                    @endif
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
            </form>
        </div>
    </div>
@endsection

@section ('script')
    <script type="text/javascript">
        $(document).ready(function() {
            $.fn.select2.defaults.set('theme', 'bootstrap')

            $('.select2').select2({
                placeholder: 'Pilih Role',
                width: null
            })

            @if (checkSubRoute('show'))
                disabledInput()
            @endif
        })
    </script>
@endsection
