<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateElectionFigureTable extends Migration
{
    public function up()
    {
        Schema::create('election_figure', function (Blueprint $table) {
            $table->integer('election_id')->unsigned();
            $table->integer('figure_id')->unsigned();

            $table->foreign('election_id')->references('id')->on('elections')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->foreign('figure_id')->references('id')->on('figures')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->primary(['election_id', 'figure_id']);
        });
    }

    public function down()
    {
        Schema::dropIfExists('election_figure');
    }
}
